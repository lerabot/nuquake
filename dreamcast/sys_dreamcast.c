/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#include <kos.h>
#include "quakedef.h"
#include "errno.h"

/*
===============================================================================

FILE IO

===============================================================================
*/

#define MAX_HANDLES             10
FILE    *sys_handles[MAX_HANDLES];

int             findhandle (void)
{
	int             i;
	
	for (i=1 ; i<MAX_HANDLES ; i++)
		if (!sys_handles[i])
			return i;
	Sys_Error ("out of handles");
	return -1;
}

/*
================
filelength
================
*/
int filelength (FILE *f)
{
	int             pos;
	int             end;

	pos = ftell (f);
	fseek (f, 0, SEEK_END);
	end = ftell (f);
	fseek (f, pos, SEEK_SET);

	return end;
}

int Sys_FileOpenRead (char *path, int *hndl)
{
	FILE    *f;
	int             i;
	
	i = findhandle ();

	f = fopen(path, "rb");
	if (!f)
	{
		*hndl = -1;
		return -1;
	}
	sys_handles[i] = f;
	*hndl = i;

	return filelength(f);
}

int Sys_FileOpenWrite (char *path)
{
	FILE    *f;
	int             i;
	
	i = findhandle ();

	f = fopen(path, "wb");
	if (!f)
		Sys_Error ("Error opening %s: %s", path,strerror(errno));
	sys_handles[i] = f;
	
	return i;
}

void Sys_FileClose (int handle)
{
	fclose (sys_handles[handle]);
	sys_handles[handle] = NULL;
}

void Sys_FileSeek (int handle, int position)
{
	fseek (sys_handles[handle], position, SEEK_SET);
}

int Sys_FileRead (int handle, void *dest, int count)
{
	return fread (dest, 1, count, sys_handles[handle]);
}

int Sys_FileWrite (int handle, void *data, int count)
{
	return fwrite (data, 1, count, sys_handles[handle]);
}

int     Sys_FileTime (char *path)
{
	FILE    *f;

	f = fopen(path, "rb");
	if (f)
	{
		fclose(f);
		return 1;
	}

	return -1;
}

void Sys_mkdir (char *path)
{
}


/*
===============================================================================

SYSTEM IO

===============================================================================
*/

void Sys_MakeCodeWriteable (unsigned long startaddr, unsigned long length)
{
}


void Sys_Error (char *error, ...)
{
	va_list         argptr;

	printf ("Sys_Error: ");   
	va_start (argptr,error);
	vprintf (error,argptr);
	va_end (argptr);
	printf ("\n");

	Sys_Quit();
}

void Sys_Printf (char *fmt, ...)
{
	va_list         argptr;
	
	va_start (argptr,fmt);
	vprintf (fmt,argptr);
	va_end (argptr);
}

void Sys_Quit (void)
{
	Host_Shutdown();
	arch_menu();
}

#include <sys/time.h>

double Sys_FloatTime(void)
{
	struct timeval tp;
    struct timezone tzp; 
    static int      secbase; 
    
    gettimeofday(&tp, &tzp);  

    if (!secbase)
    {
        secbase = tp.tv_sec;
        return tp.tv_usec/1000000.0;
    }

    return (tp.tv_sec - secbase) + tp.tv_usec/1000000.0;
}

char *Sys_ConsoleInput (void)
{
	return NULL;
}

void Sys_Sleep (void)
{
}

qboolean	isDedicated = false;

//-----------------------------------------------------------------------------
static void drawtext(int x, int y, char *string)
{
	printf("%s\n", string);
	int offset = ((y * 640) + x);
	bfont_draw_str(vram_s + offset, 640, 1, string);
}

static void assert_hnd(const char *file, int line, const char *expr, const char *msg, const char *func)
{
	char strbuffer[1024];

	/* Reset video mode, clear screen */
	vid_set_mode(DM_640x480, PM_RGB565);
	vid_empty();

	/* Display the error message on screen */
	drawtext(32, 64, "nuQuake - Assertion failure");

	sprintf(strbuffer, " Location: %s, line %d (%s)", file, line, func);
	drawtext(32, 96, strbuffer);

	sprintf(strbuffer, "Assertion: %s", expr);
	drawtext(32, 128, strbuffer);

	sprintf(strbuffer, "  Message: %s", msg);
	drawtext(32, 160, strbuffer);

	/* Hang - infinite loop */
	while (1)
		;
}

//=============================================================================

KOS_INIT_FLAGS(INIT_NONE | INIT_NO_DCLOAD | INIT_IRQ | INIT_THD_PREEMPT);

void main (int argc, char **argv)
{
	// Set up assertion handler
	assert_set_handler(assert_hnd);

	GLdcConfig config;
	glKosInitConfig(&config);

	config.initial_op_capacity = 8096;
	config.initial_pt_capacity = 2048;
	config.initial_tr_capacity = 4096;
	config.initial_immediate_capacity = 2096;

	glKosInitEx(&config);

	static quakeparms_t parms;
	double time, oldtime, newtime;

	char *basedirs[6]={
		"/cd/QUAKE", /* installed  */
		"/cd/QUAKE_SW", /* shareware */
		"/cd/data", /* official CD-ROM */
		"/pc/quake", /* debug */
		"/pc/quake_sw", /* debug */
		NULL
	};

	char *basedir;
	
#if 0
	int i;
	
	for(i=0;(basedir = basedirs[i])!=NULL;i++) {
		int fd  = fs_open(basedir,O_DIR);
		if (fd!=0) {
			fs_close(fd);
			break;
		}
	}
	if (basedir==NULL)
		Sys_Error("can't find quake dir");
#else
	{
	static char *args[10] = {"quake",NULL};
	argc = 1;
	argv = args;
	basedir = menu(&argc,argv,basedirs, 6);
	}
#endif

	parms.memsize = 8 * 1024 * 1024;
	parms.membase = malloc (parms.memsize);
	sq_clr(parms.membase, parms.memsize);
	//parms.basedir = "/cd/QUAKE";
	parms.basedir = basedir;

	COM_InitArgv (argc, argv);

	parms.argc = com_argc;
	parms.argv = com_argv;

	printf ("Host_Init\n");
	Host_Init (&parms);
	oldtime = Sys_FloatTime() - 0.1;
	//profiler_enable();
	while (1)
	{
		newtime = Sys_FloatTime();
		time = newtime - oldtime;

		Host_Frame(time);
		oldtime = newtime;
	}
}

char *get_savedir(void)
{
	static char savedir[]="/vmu/a1";
	#if 0
	int port,unit;
	uint8 addr;
	addr = maple_first_vmu();
	if (addr==0) return "dmy";
	maple_raddr(addr,&port,&unit);
	savedir[5] = 'a' + port;
	savedir[6] = '0' + unit;
	#endif
	return savedir;
}
