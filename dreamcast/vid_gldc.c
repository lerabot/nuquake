/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// vid_null.c -- null video driver to aid porting efforts

#include "quakedef.h"

#include <dc/pvr.h>
#include <GL/glext.h>
#include <dc/sq.h>

void glColor3ubv(GLubyte *v) {
    glColor4f((float)v[0]/255,(float)v[1]/255,(float)v[2]/255,1.0f);
}

viddef_t	vid;				// global video state

#define	BASEWIDTH	640
#define	BASEHEIGHT	480

unsigned int  d_8to24table[256];
unsigned char d_15to8table[1<<15];
unsigned char *newPalette;
unsigned char *lightingPalette;

/* Cvars we set to give default acceptable performance */
extern cvar_t	gl_cull;
extern cvar_t	gl_picmip;
extern cvar_t	gl_smoothmodels;
extern cvar_t	gl_affinemodels;
extern cvar_t	show_fps;
extern cvar_t 	r_dynamic;
extern cvar_t	gl_subdivide_size;
extern cvar_t	gl_keeptjunctions;
extern cvar_t	gl_triplebuffer;
extern cvar_t	r_speeds;

int		texture_mode = GL_NEAREST;
 
int		texture_extension_number = 1;

float		gldepthmin, gldepthmax;

/* Custom Cvars we are Adding */
cvar_t	gl_ztrick = {"gl_ztrick","0"};
cvar_t	r_sky = {"r_sky","0"};
cvar_t	cull_dist = {"dist","650.0"};
cvar_t	gl_lighting = {"gl_lighting", "1"};

const char *gl_vendor;
const char *gl_renderer;
const char *gl_version;
const char *gl_extensions;

qboolean is8bit = true;
qboolean isPermedia = false;
qboolean isDreamcast = true;
volatile qboolean gl_mtexable = false;

/* Used for generating the lightmap multiple ways for debugging */

void VID_GenerateLighting_f (void)
{
	if (Cmd_Argc() < 1)
		return;

	VID_GenerateLighting((byte)Q_atoi(Cmd_Argv (1)));
}

void VID_ChangeLightmapFilter_f (void)
{
	extern int lightmap_filter;
	if (Cmd_Argc() < 1)
		return;

	if( (byte)Q_atoi(Cmd_Argv (1)) ){
		lightmap_filter = GL_LINEAR;
	}else {
		lightmap_filter = GL_NEAREST;
	}
}



void VID_GenerateLighting(qboolean alpha){
	lightingPalette = (unsigned char*)malloc(256*4);
	int i,j;

	for(i=0;i<16;i++){
		for(j=0;j<16;j++){
		if(alpha){
			lightingPalette[(i * 16 + j) * 4] = 	0x0;
			lightingPalette[(i * 16 + j) * 4 + 1] = 0x0;
			lightingPalette[(i * 16 + j) * 4 + 2] = 0x0;
			lightingPalette[(i * 16 + j) * 4 + 3] = i * 16 + j;
		} else {
			lightingPalette[(i * 16 + j) * 4] = 	i * 16 + j;
			lightingPalette[(i * 16 + j) * 4 + 1] = i * 16 + j;
			lightingPalette[(i * 16 + j) * 4 + 2] = i * 16 + j;
			lightingPalette[(i * 16 + j) * 4 + 3] = 0xFF;
		}
		}
	}
	glColorTableEXT(GL_SHARED_TEXTURE_PALETTE_1_KOS, GL_RGBA8, 256, GL_RGBA, GL_UNSIGNED_BYTE, (void *) lightingPalette );
	free(lightingPalette);
}

void	VID_SetPalette (unsigned char *palette)
{
	byte	*pal;
	unsigned r,g,b;
	unsigned v;
	int     r1,g1,b1;
	int		j,k,l,m;
	unsigned short i;
	unsigned	*table;
	char s[255];
	int dist, bestdist;
	static qboolean palflag = false;
	newPalette = (unsigned char*)malloc(256*4);

//
// 8 8 8 encoding
//


	pal = palette;
	table = d_8to24table;
	for (i=0 ; i<256 ; i++)
	{
		r = pal[0];
		g = pal[1];
		b = pal[2];
		pal += 3;

		newPalette[i * 4] = r;
        newPalette[i * 4 + 1] = g;
        newPalette[i * 4 + 2] = b;
        newPalette[i * 4 + 3] = 0xFF;

		v = (255<<24) + (r<<0) + (g<<8) + (b<<16);
		*table++ = v;
	}
	d_8to24table[255] &= 0xffffff;	// 255 is transparent
	unsigned int *pfix = newPalette; 
	pfix[255] &= 0xffffff;

	// JACK: 3D distance calcs - k is last closest, l is the distance.
	for (i=0; i < (1<<15); i++) {
		/* Maps
		000000000000000
		000000000011111 = Red  = 0x1F
		000001111100000 = Blue = 0x03E0
		111110000000000 = Grn  = 0x7C00
		*/
		r = ((i & 0x1F) << 3)+4;
		g = ((i & 0x03E0) >> 2)+4;
		b = ((i & 0x7C00) >> 7)+4;
		pal = (unsigned char *)d_8to24table;
		for (v=0,k=0,bestdist=10000*10000; v<256; v++,pal+=4) {
			r1 = (int)r - (int)pal[0];
			g1 = (int)g - (int)pal[1];
			b1 = (int)b - (int)pal[2];
			dist = (r1*r1)+(g1*g1)+(b1*b1);
			if (dist < bestdist) {
				k=v;
				bestdist = dist;
			}
		}
		d_15to8table[i]=k;
	}
}

void	VID_ShiftPalette (unsigned char *palette)
{
//	VID_SetPalette(palette);
}

void VID_Init8bitPalette(void)
{
	Con_SafePrintf("... Using GL_EXT_shared_texture_palette\n");
	glEnable( GL_SHARED_TEXTURE_PALETTE_EXT );
	glColorTableEXT(GL_SHARED_TEXTURE_PALETTE_EXT, GL_RGBA8, 256, GL_RGBA, GL_UNSIGNED_BYTE, (void *) newPalette );
	free(newPalette);
	VID_GenerateLighting(true);
	is8bit = true;
}


void VID_Shutdown (void)
{

}

qboolean VID_Is8bit(void)
{
	return is8bit;
}

void CheckMultiTextureExtensions(void) 
{
		qglMTexCoord2fSGIS = (void *) &glMultiTexCoord2fARB;
		qglSelectTextureSGIS = (void *) &glActiveTextureARB;
}

extern int		gl_filter_min;
extern int		gl_filter_max;

/*
===============
GL_Init
===============
*/
void GL_Init (void)
{
	gl_vendor = glGetString (GL_VENDOR);
	Con_Printf ("GL_VENDOR: %s\n", gl_vendor);
	gl_renderer = glGetString (GL_RENDERER);
	Con_Printf ("GL_RENDERER: %s\n", gl_renderer);

	gl_version = glGetString (GL_VERSION);
	Con_Printf ("GL_VERSION: %s\n", gl_version);
	gl_extensions = glGetString (GL_EXTENSIONS);

	Con_Printf("\nThanks to everyone EXCEPT Saturn-Tan!\n\n");

	//glKosReserveOPList(8192);

	CheckMultiTextureExtensions();

	glClearColor (1,1,1,1);
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.666f);

	//glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
	glShadeModel (GL_FLAT);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

/*
=================
GL_BeginRendering

=================
*/
void GL_BeginRendering (int *x, int *y, int *width, int *height)
{
	*x = *y = 0;
	*width = BASEWIDTH;
	*height = BASEHEIGHT;
	//gl_mtexable = (int)r_sky.value;
}
char st[80];

void GL_EndRendering (void)
{
	int x,y;
	x = vid.width/2 - strlen(st) * 16;
	y = vid.height-SBAR_HEIGHT-24;
	Draw_String(x, y, st);
	x = vid.width - strlen(st) * 16 - 16;
	y = 0;
	Draw_String(x, y, st);

	/* I believe this mirrors the Windows driver, otherwise status bar flicker */
	Sbar_Changed();
	glKosSwapBuffers();
}

void VID_Cvar_Update (void)
{
	//Cvar Changes
	gl_smoothmodels.value	= 0;
	gl_affinemodels.value	= 1;
	//r_dynamic.value 		= 1;
	gl_keeptjunctions.value = 0;
	gl_triplebuffer.value 	= 0;
	gl_cull.value = 0;
	
	show_fps.value = 1;
}

void VID_Init (unsigned char *palette)
{
	Cvar_RegisterVariable (&gl_ztrick);
	Cvar_RegisterVariable (&gl_lighting);
	Cvar_RegisterVariable (&r_sky);
	Cvar_RegisterVariable (&cull_dist);

	Cmd_AddCommand("genlmp", VID_GenerateLighting_f);
	Cmd_AddCommand("lmpquality", VID_ChangeLightmapFilter_f);

	vid.width =  BASEWIDTH;
	vid.height = BASEHEIGHT;
	/*vid.conwidth = 320;	
	vid.conwidth &= 0xfff8; // make it a multiple of eight
	if (vid.conwidth < 320)
		vid.conwidth = 320;
	// pick a conheight that matches with correct aspect
	vid.conheight = vid.conwidth*3 / 4;*/
	vid.aspect = 1.0;
	vid.numpages = 1;
	//memcpy (vid.colormap, host_colormap, VID_GRADES * 256);
	vid.colormap = host_colormap;
	vid.fullbright = 256 - LittleLong (*((int *)vid.colormap + 2048));
	vid.rowbytes = vid.conrowbytes = BASEWIDTH/2;

	sprintf(st, "%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c",65, 76, 80, 72, 65, 32, 110, 117, 81, 117, 97, 107, 101, 32, 65, 76, 80, 72, 65 );

	GL_Init();

	VID_SetPalette(palette);

	VID_Init8bitPalette();

	vid.recalc_refdef = 1;				// force a surface cache flush

	VID_Cvar_Update();
}
