BASE_CFLAGS = -Dstricmp=strcasecmp -Dstrnicmp=strncasecmp -D_stricmp=strcasecmp -D_strnicmp=strncasecmp -DDC
RELEASE_CFLAGS = $(BASE_CFLAGS) -ffast-math -O3
CFLAGS = $(RELEASE_CFLAGS) -Wall -DUSE_ZLIB -DGLQUAKE

CC = $(KOS_CC)

LIBS = -lGLdc -lz
INCS = -iquote common

default: glquake.elf
all : glquake.elf

CLIENT_OBJS = client/cl_demo.o client/cl_input.o client/cl_main.o client/cl_parse.o \
    client/cl_tent.o client/view.o client/keys.o client/menu.o 

COMMON_OBJS = common/cmd.o common/chase.o common/common.o common/console.o \
    common/crc.o common/cvar.o common/host.o common/host_cmd.o common/mathlib.o  common/sbar.o \
    common/wad.o common/world.o common/zone.o
	
NET_OBJS = common/net_dgrm.o common/net_loop.o common/net_main.o common/net_vcr.o

VM_OBJS = vm/pr_cmds.o vm/pr_edict.o vm/pr_exec.o

SERVER_OBJS = server/sv_main.o server/sv_move.o server/sv_phys.o server/sv_user.o 

GL_OBJS = renderer/gl_draw.o renderer/gl_mesh.o renderer/gl_model.o renderer/gl_screen.o renderer/gl_refrag.o  renderer/gl_rlight.o \
	renderer/gl_rmain.o renderer/gl_rmisc.o renderer/gl_rsurf.o renderer/gl_warp.o renderer/r_part.o 
# renderer/gl_batcher.o

DC_OBJS =  dreamcast/in_dreamcast.o dreamcast/net_none.o dreamcast/sys_dreamcast.o dreamcast/vid_gldc.o	dreamcast/menu_dreamcast.o dreamcast/vmuheader.c

# Null sound for speed testing
#DC_SND = null/snd_null.o null/cd_null.o
DC_SND = dreamcast/snddma_dreamcast.o dreamcast/snd_mix_dreamcast.o client/snd_dma.o client/snd_mem.o dreamcast/cd_kos.o dreamcast/aica.o 
# dreamcast/cd_dreamcast.o dreamcast/fake_cdda.o

GLDCOBJS = $(CLIENT_OBJS) $(DC_SND) $(COMMON_OBJS) $(NET_OBJS) $(VM_OBJS) $(SERVER_OBJS) $(GL_OBJS) $(DC_OBJS)

# if need to check memory layout
#-Xlinker -Map=MAP_bin1.txt
glquake.elf : $(GLDCOBJS)
	@echo  $@
	$(KOS_CC) $(INCS) $(KOS_CFLAGS) $(CFLAGS) $(LDFLAGS) $(KOS_LDFLAGS) -o glquake.elf  $(KOS_START) $(GLDCOBJS) -L$(KOS_BASE)/lib $(LIBS) -lm $(KOS_LIBS)
	@$(KOS_CC_BASE)/sh-elf/bin/objcopy -R .stack -O binary glquake.elf glquake
	@$(KOS_BASE)/utils/scramble/scramble glquake 1ST_READ.BIN

refresh : 
	@rm -f glquake.elf
	@make
    
%.o: %.c
	@echo  $<
	@$(KOS_CC) $(INCS) $(KOS_CFLAGS) $(CFLAGS) $(LDFLAGS) -c $< -o $@

%.o: common/%.c
	@echo  $<
	@$(KOS_CC) $(INCS) $(KOS_CFLAGS) $(CFLAGS) $(LDFLAGS) -c $< -o $@

%.o: client/%.c
	@echo  $<
	@$(KOS_CC) $(INCS) $(KOS_CFLAGS) $(CFLAGS) $(LDFLAGS) -c $< -o $@
    
%.o: renderer/%.c
	@echo  $<
	@$(KOS_CC) $(INCS) $(KOS_CFLAGS) $(CFLAGS) $(LDFLAGS) -c $< -o $@

%.o: dreamcast/%.c
	@echo  $<
	@$(KOS_CC) $(INCS) $(KOS_CFLAGS) $(CFLAGS) $(LDFLAGS) -c $< -o $@
    
%.o: vm/%.c
	@echo  $<
	@$(KOS_CC) $(INCS) $(KOS_CFLAGS) $(CFLAGS) $(LDFLAGS) -c $< -o $@

# clean house
clean:
	rm -f $(GLDCOBJS) $(glquake.elf) 1ST_READ.BIN glquake
